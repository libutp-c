#ifndef __TEMPLATES_H__
#define __TEMPLATES_H__

#include "utypes.h"
#include <assert.h>

#ifdef __GNUC__
/* Used for gcc tool chains accepting but not supporting pragma pack
   See http://gcc.gnu.org/onlinedocs/gcc/Type-Attributes.html */
#define PACKED_ATTRIBUTE __attribute__((__packed__))
#elif defined __TINYC__
/* See http://bellard.org/tcc/tcc-doc.html#TOC8 */
#define PACKED_ATTRIBUTE __attribute__((__packed__))
#else
#define PACKED_ATTRIBUTE
#endif

#ifdef __GNUC__
#define ALIGNED_ATTRIBUTE(x)  __attribute__((aligned (x)))
#elif defined __TINYC__
/* See http://bellard.org/tcc/tcc-doc.html#TOC8 */
#define ALIGNED_ATTRIBUTE(x)  __attribute__((aligned (x)))
#else
#define ALIGNED_ATTRIBUTE(x)
#endif

/*----------------------------------------------------------------------------*/
#define MAX_TMPL(T)			\
static __inline__ T T ## _max(T a, T b)	\
{					\
        if (a > b)			\
                return a;		\
        return b;			\
}

MAX_TMPL(int32)

MAX_TMPL(uint)

MAX_TMPL(size_t)
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#define MIN_TMPL(T)			\
static __inline__ T T ## _min(T a, T b)	\
{					\
        if (a < b)			\
                return a;		\
        return b;			\
}

MIN_TMPL(uint32)

MIN_TMPL(uint)

MIN_TMPL(size_t)

MIN_TMPL(int64)

MIN_TMPL(socklen_t)
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#define CLAMP_TMPL(T)					\
static __inline__ T T ## _clamp(T v, T mi, T ma)	\
{							\
	if (v > ma) v = ma;				\
	if (v < mi) v = mi;				\
	return v;					\
}

CLAMP_TMPL(size_t)
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static __inline__ size_t size_t_min3(size_t a, size_t b, size_t c)
{
	return size_t_min(size_t_min(a,b),c);
}
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
#define ARRAY_MINSIZE 16
struct array {
	void *mem;
	size_t alloc;
	size_t count;
	size_t type_bytes_n;/* inited once */
};

static __inline__ size_t ar_GetCount(struct array *ar)
{
	return ar->count;
}

static __inline__ size_t ar_GetAlloc(struct array *ar)
{
	return ar->alloc;
}

static __inline__ void ar_SetCount(struct array *ar, size_t c)
{
	ar->count = c;
}

/* operator[] */
static __inline__ void *ar_get(struct array *ar, size_t offset)
{
	byte *mem;
	assert(offset ==0 || offset <ar->alloc);
	mem = ar->mem;
	return mem + offset * ar->type_bytes_n;
}

static __inline__ void ar_Resize(struct array *ar, size_t a)
{
	if (a == 0) {
		free(ar->mem);

		ar->mem = NULL;
		ar->alloc = 0;
		ar->count = 0;
	} else {
		ar->alloc = a;
		ar->mem = realloc(ar->mem, ar->alloc * ar->type_bytes_n);
	}
}

static __inline__ void ar_Grow(struct array *ar)
{
	ar_Resize(ar, size_t_max(ARRAY_MINSIZE, ar->alloc * 2));
}

/* value copy in the array */
static __inline__ size_t ar_Append(struct array *ar, void *t)
{
	size_t r;
	byte *mem;

	if (ar->count >= ar->alloc)
		ar_Grow(ar);
	r = ar->count++;
	mem = ar->mem;
	memcpy(mem + r * ar->type_bytes_n, t, ar->type_bytes_n);
	return r;
}

/* get a pointer on an empty slot in the array */
static __inline__ void *ar_Append_new(struct array *ar)
{
	byte *mem;

	if (ar->count >= ar->alloc)
		ar_Grow(ar);
	mem = ar->mem;
	return mem + ar->count++ * ar->type_bytes_n;
}

static __inline__ void ar_Compact(struct array *ar)
{
	ar_Resize(ar, ar->count);
}

static __inline__ bool ar_MoveUpLast(struct array *ar, size_t index)
{
	size_t c;

	assert(index < ar->count);

	c = --ar->count;
	if (index != c) {
		byte *mem;

		mem = ar->mem;
		memcpy(mem + index * ar->type_bytes_n,
				mem + c * ar->type_bytes_n, ar->type_bytes_n);
		return true;
	}
	return false;
}
/*----------------------------------------------------------------------------*/
#endif /* __TEMPLATES_H__ */
