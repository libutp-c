#!/bin/sh

#defaulted to tinycc
CC=tcc
AR=ar

$CC -c -DPOSIX -I./ -o utp.o utp.c
$CC -c -DPOSIX -I./ -o utp_utils.o utp_utils.c

$AR rs libutp.a utp.o utp_utils.o
